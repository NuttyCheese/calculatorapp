//
//  CalculatorViewController.swift
//  CalculatorApp
//
//  Created by Борис Павлов on 02.10.2023.
//

import UIKit

final class CalculatorViewController: UIViewController {
    private let resultLabel = UILabel()
    private var defaultCollectionView: UICollectionView!
    
    private let model = CalculatorTypeModel()
    private let logic = CalculatorLogic()
    
    private var firstValue: Int = 0
    private var secondValue: Double = 0
    private var actionCalculations = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
}

// MARK: - UICollectionViewDelegate
extension CalculatorViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let title = model.defaultTypeArray[indexPath.item]
        switch title {
        case "0","1","2","3","4","5","6","7","8","9":
            guard let number = Int(title) else { return }
            updateDisplayWithNumber(number)
        default:
            logic.performAction(title, with: resultLabel.text ?? "")
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CalculatorViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, 
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        calculateItemSize(for: indexPath, in: collectionView)
    }
    /// Расчитывает размер элемента коллекции
    ///
    /// - Parameters:
    ///   - indexPath: Индекс элемента коллекции
    ///   - collectionView: Коллекция, в которой находится элемент
    /// - Returns: Размер элемента коллекции
    private func calculateItemSize(for indexPath: IndexPath, in collectionView: UICollectionView) -> CGSize {
        let numberOfItemsPerRow: Int = 4
        let numberOfItemsPerColumn: Int = 5
        let horizontalSpacing: CGFloat = 1
        let verticalSpacing: CGFloat = 1
        let totalHorizontalSpacing = CGFloat(numberOfItemsPerRow - 1) * horizontalSpacing
        let totalVerticalSpacing = CGFloat(numberOfItemsPerColumn - 1) * verticalSpacing
        let itemWidth = (collectionView.bounds.width - totalHorizontalSpacing) / CGFloat(numberOfItemsPerRow)
        let itemHeight = (collectionView.bounds.height - totalVerticalSpacing) / CGFloat(numberOfItemsPerColumn)
        
        if indexPath.item == 16 {
            return CGSize(width: itemWidth * 2 + horizontalSpacing + 2, height: itemHeight)
        } else {
            return CGSize(width: itemWidth, height: itemHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, 
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat { // vertical
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, 
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat { // horizontal
        0
    }
}

// MARK: - UICollectionViewDataSource
extension CalculatorViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        model.defaultTypeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CalculatorViewCell = collectionView.dequeueReusableCell(for: indexPath)
        let index = model.defaultTypeArray[indexPath.row]
        
        switch index {
        case "+", "-", "x", "÷", "=":
            cell.backgroundColor = .orange.withAlphaComponent(0.4)
        case "1","2","3","4","5","6","7","8","9","0":
            cell.backgroundColor = .gray.withAlphaComponent(0.4)
        default:
            cell.backgroundColor = .darkGray.withAlphaComponent(0.4)
        }
        
        cell.configure(with: index)
        cell.basicSettings()
        
        return cell
    }
}
// MARK: - Private Methods
private extension CalculatorViewController {
    /// Настраивает основной вид контроллера
    func setupView() {
        view.backgroundColor = .white
        
        setupDefaultCollectionView()
        setupResultLabel()
        
        view.subviewsOnView(defaultCollectionView, resultLabel)
        
        logic.displayValueUpdate = { [weak self] updateValue in
            guard let self else { return }
            self.resultLabel.text = updateValue
        }
        
        setupConstraints()
    }
    
    /// Настраивает коллекцию (сетки) кнопок калькулятора
    func setupDefaultCollectionView() {
        let layout = UICollectionViewFlowLayout()
        defaultCollectionView = UICollectionView(frame: view.bounds, collectionViewLayout: layout)
        
        defaultCollectionView.delegate = self
        defaultCollectionView.dataSource = self
        defaultCollectionView.isScrollEnabled = false
        defaultCollectionView.showsVerticalScrollIndicator = false
        defaultCollectionView.showsHorizontalScrollIndicator = false
        
        defaultCollectionView.registrationCells(CalculatorViewCell.self)
    }
    
    /// Настраивает метку для отображения результата
    func setupResultLabel() {
        resultLabel.applyStyle(.basicSettings)
        
        resultLabel.layer.cornerRadius = 10
        resultLabel.layer.borderColor = UIColor.white.cgColor
        resultLabel.layer.borderWidth = 1.5
        resultLabel.clipsToBounds = true
    }
    
    /// Обновляет отображение с введенным числом
    /// - Parameter number: Введенное число
    func updateDisplayWithNumber(_ number: Int) {
        firstValue = number
        if let currentValue = resultLabel.text {
            resultLabel.text = currentValue + "\(firstValue)"
        } else {
            resultLabel.text = "\(firstValue)"
        }
    }
    
    /// Настраивает констрейнты для элементов интерфейса
    func setupConstraints() {
        defaultCollectionView.const(.full(top: view.bounds.height / 2,
                                          left: 1.5,
                                          right: -1.5,
                                          bottom: -1.5))
        resultLabel.tAMIC()
        
        NSLayoutConstraint.activate([
            resultLabel.bottomAnchor.constraint(equalTo: defaultCollectionView.topAnchor, constant: -2),
            resultLabel.widthAnchor.constraint(equalTo: defaultCollectionView.widthAnchor, multiplier: 0.9),
            resultLabel.centerXAnchor.constraint(equalTo: defaultCollectionView.centerXAnchor),
            resultLabel.heightAnchor.constraint(equalTo: defaultCollectionView.heightAnchor, multiplier: 0.18)
        ])
    }
}

