//
//  CalculatorViewCell.swift
//  CalculatorApp
//
//  Created by Борис Павлов on 12.10.2023.
//

import UIKit

final class CalculatorViewCell: UICollectionViewCell {
    private let titleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CalculatorViewCell: ICollectionViewCell {
    func configure(with model: Any) {
        guard let model = model as? String else { return }
        
        titleLabel.text = model
    }
}

private extension CalculatorViewCell {
    func setupView() {
        titleLabel.applyStyle(.basicSettings)
        
        contentView.subviewsOnView(titleLabel)
        titleLabel.const(.full(top: 0.5, left: 0.5,
                               right: -0.5, bottom: -0.5)
        )
    }
}
