//
//  CalculatorLogic.swift
//  CalculatorApp
//
//  Created by Борис Павлов on 09.10.2023.
//

import Foundation
/// Класс для выполнения вычислений калькулятора
final class CalculatorLogic {
    private var actionCalculations: String?
    private var secondValue: Double = 0
    
    public var displayValueUpdate: ((String?) -> ())?
    
    /// Выполняет указанное действие с текущим значением
    /// - Parameters:
    ///   - action: Действие, которое нужно выполнить (например, "+", "-", "x", "÷", "AC" и т.д.)
    ///   - value: Текущее значение в строковом формате
    public func performAction(_ action: String, with value: String) {
        let formatter = createNumberFormatter()
        
        // Преобразуем запятую в точку для корректного преобразования строки в Double
        let normalizedValue = value.replacingOccurrences(of: ",", with: ".")
        guard let doubleValue = Double(normalizedValue) else { return }
        
        switch action {
        case "+", "-", "x", "÷":
            handleBasicOperations(action, with: doubleValue)
        case "AC":
            resetCalculator()
        case ",":
            handleDecimalSeparator(for: doubleValue, with: formatter)
        case "±":
            handlePlusMinus(for: doubleValue, with: formatter)
        case "%":
            handlePercentage(for: doubleValue, with: formatter)
        case "=":
            handleEqual(with: doubleValue, and: formatter)
        default:
            break
        }
    }
}

private extension CalculatorLogic {
    /// Создает и возвращает настроенный экземпляр NumberFormatter
    /// - Returns: Настроенный экземпляр NumberFormatter
    func createNumberFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 30
        formatter.numberStyle = .decimal
        formatter.decimalSeparator = ","
        return formatter
    }
    
    /// Обрабатывает базовые операции (сложение, вычитание, умножение, деление)
    /// - Parameters:
    ///   - action: Действие, которое нужно выполнить (например, "+", "-", "x", "÷")
    ///   - value: Текущее значение в формате Double
    func handleBasicOperations(_ action: String, with value: Double) {
        secondValue = value
        displayValueUpdate?("")
        actionCalculations = action
    }
    
    /// Сбрасывает значения калькулятора
    func resetCalculator() {
        secondValue = 0
        actionCalculations = nil
        displayValueUpdate?("")
    }
    
    /// Обрабатывает десятичный разделитель
    /// - Parameters:
    ///   - value: Текущее значение в формате Double
    ///   - formatter: Настроенный экземпляр NumberFormatter
    func handleDecimalSeparator(for value: Double, with formatter: NumberFormatter) {
        if let formattedString = formatter.string(from: NSNumber(value: value)), !formattedString.contains(",") {
            let newFormattedString = formattedString + ","
            displayValueUpdate?(newFormattedString)
        }
    }
    
    /// Обрабатывает изменение знака числа
    /// - Parameters:
    ///   - value: Текущее значение в формате Double
    ///   - formatter: Настроенный экземпляр NumberFormatter
    func handlePlusMinus(for value: Double, with formatter: NumberFormatter) {
        let newValue = -value
        displayValueUpdate?(formatter.string(from: NSNumber(value: newValue)))
    }
    
    /// Обрабатывает вычисление процента
    /// - Parameters:
    ///   - value: Текущее значение в формате Double
    ///   - formatter: Настроенный экземпляр NumberFormatter
    func handlePercentage(for value: Double, with formatter: NumberFormatter) {
        let percentValue = value / 100
        formatter.maximumFractionDigits = percentValue.truncatingRemainder(dividingBy: 1) == 0 ? 0 : 2
        displayValueUpdate?(formatter.string(from: NSNumber(value: percentValue)))
    }
    
    /// Обрабатывает выполнение операции "="
    /// - Parameters:
    ///   - value: Текущее значение в формате Double
    ///   - formatter: Настроенный экземпляр NumberFormatter
    func handleEqual(with value: Double, and formatter: NumberFormatter) {
        guard let actionCalculations = actionCalculations else { return }
        
        let equalValue: Double?
        switch actionCalculations {
        case "+":
            equalValue = secondValue + value
        case "-":
            equalValue = secondValue - value
        case "x":
            equalValue = secondValue * value
        case "÷":
            equalValue = secondValue / value
        default:
            equalValue = nil
        }
        
        if let equalValue = equalValue {
            displayValueUpdate?(formatter.string(from: NSNumber(value: equalValue)))
            secondValue = equalValue
        }
    }
}
