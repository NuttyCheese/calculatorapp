//
//  ModelStyle.swift
//  CalculatorApp
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

enum Color {
    case black(_ : UIColor = .black)
    case white(_ : UIColor = .white)
}

enum Font {
    case regular(_ : UIFont = UIFont.systemFont(ofSize: 32))
    case bold(_ : UIFont = UIFont.boldSystemFont(ofSize: 32))
}

enum Alignment {
    case center(_ : NSTextAlignment = .center)
}

enum NumberOfLines {
    case neverMind(_ : Int = 0)
}

struct ModelStyle {
    let textColor: Color
    let font: Font
    let alignment: Alignment
    let nOL: NumberOfLines
    
    static let basicSettings = ModelStyle(textColor: .black(), font: .regular(), alignment: .center(), nOL: .neverMind())
}

extension UILabel {
    func applyStyle(_ style: ModelStyle) {
        switch style.textColor {
            case .black(let color): textColor = color
            case .white(let color): textColor = color
        }
        
        switch style.font {
            case .regular(let font): self.font = font
            case .bold(let font): self.font = font
        }
        
        switch style.alignment {
            case .center(let alignment): textAlignment = alignment
        }
        
        switch style.nOL {
            case .neverMind(let nOL): numberOfLines = nOL
        }
    }
}
