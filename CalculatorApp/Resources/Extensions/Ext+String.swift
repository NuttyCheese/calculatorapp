//
//  Ext+String.swift
//  CalculatorApp
//
//  Created by Борис Павлов on 15.06.2024.
//

import UIKit

extension String {
    /**
     Создает атрибутированную строку с основанием и показателем степени.
     
     - Parameters:
     - base: Строка основания.
     - exponent: Строка показателя степени.
     - position: Позиция смещения показателя степени по вертикали относительно основания. По умолчанию равно 1.
     
     - Returns: NSAttributedString с основанием и показателем степени.
     
     Пример использования:
     ```swift
     let degreeTitle = "".setDegreeTitle(base: "x", exponent: "2")
     ```
     */
    func setDegreeTitle(base: String, exponent: String, position: CGFloat = 1) -> NSAttributedString {
        // Создаем атрибутированную строку с основанием и задаем для нее шрифт
        let attributedString = NSMutableAttributedString(string: base, attributes: [
            .font: UIFont.boldSystemFont(ofSize: 16)
        ])
        
        // Задаем шрифт для показателя степени
        let smallFont = UIFont.boldSystemFont(ofSize: 10)
        // Определяем атрибуты для показателя степени, включая смещение базовой линии
        let smallAttributes: [NSAttributedString.Key: Any] = [.font: smallFont,
                                                              .baselineOffset: (16 - smallFont.pointSize) * position]
        // Создаем атрибутированную строку для показателя степени
        let exponentString = NSAttributedString(string: exponent, attributes: smallAttributes)
        // Добавляем показатель степени к атрибутированной строке
        attributedString.append(exponentString)
        
        return attributedString
    }
    
    /**
     Создает атрибутированную строку с символом корня и левым/правым символами.
     
     - Parameters:
     - mainSymbols: Основной символ корня. По умолчанию равно "√".
     - leftSymbol: Левый символ.
     - rightSymbol: Правый символ.
     
     - Returns: NSAttributedString с символом корня и левым/правым символами.
     
     Пример использования:
     ```swift
     let rootTitle = "".setRootTitle(leftSymbol: "3", rightSymbol: "8")
     ```
     */
    func setRootTitle(mainSymbols: String = "√", leftSymbol: String, rightSymbol: String) -> NSAttributedString {
        // Создаем пустую атрибутированную строку
        let attributedString = NSMutableAttributedString(string: "", attributes: [:])
        
        // Задаем шрифт для левого символа
        let leftSymbolSmall = UIFont.boldSystemFont(ofSize: 10)
        // Определяем атрибуты для левого символа, включая смещение базовой линии
        let leftAttributed: [NSAttributedString.Key: Any] = [.font: leftSymbolSmall,
                                                             .baselineOffset: (16 - leftSymbolSmall.pointSize) / 2]
        // Создаем атрибутированную строку для левого символа
        let leftPosition = NSAttributedString(string: leftSymbol, attributes: leftAttributed)
        
        // Задаем шрифт для правого символа
        let rightSymbolSmall = UIFont.boldSystemFont(ofSize: 10)
        // Определяем атрибуты для правого символа, включая смещение базовой линии
        let rightAttributed: [NSAttributedString.Key: Any] = [.font: rightSymbolSmall,
                                                              .baselineOffset: (16 - rightSymbolSmall.pointSize) / 2]
        // Создаем атрибутированную строку для правого символа
        let rightPosition = NSAttributedString(string: rightSymbol, attributes: rightAttributed)
        
        // Добавляем левый символ к атрибутированной строке
        attributedString.append(leftPosition)
        
        // Создаем атрибутированную строку для символа корня и задаем для нее шрифт
        let mainAttributedString = NSAttributedString(string: mainSymbols, attributes: [
            .font: UIFont.boldSystemFont(ofSize: 16)
        ])
        
        // Добавляем символ корня к атрибутированной строке
        attributedString.append(mainAttributedString)
        
        // Добавляем правый символ к атрибутированной строке
        attributedString.append(rightPosition)
        return attributedString
    }
}
