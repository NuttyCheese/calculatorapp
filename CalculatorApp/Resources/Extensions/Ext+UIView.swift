//
//  Ext+UIView.swift
//  CalculatorApp
//
//  Created by Борис Павлов on 15.06.2024.
//

import UIKit

extension UIView {
    /**
     Включает взаимодействие с пользователем для UIView.
     */
    func enable() {
        isUserInteractionEnabled = true
    }
    /**
     Отключает взаимодействие с пользователем для UIView.
     */
    func disable() {
        isUserInteractionEnabled = false
    }
    /**
     Отключает автоматическое создание ограничений для UIView.
     */
    func tAMIC() {
        translatesAutoresizingMaskIntoConstraints = false
    }
    /**
     Добавляет подвиды к текущему UIView.

     - Parameter subviews: Переменное количество подвидов, которые нужно добавить.

     Пример использования:
     ```swift
     view.subviewsOnView(subview1, subview2, subview3)
     ```
     */
    func subviewsOnView(_ subviews: UIView...) {
        subviews.forEach { addSubview($0) }
    }
}
