//
//  Ext+UICollectionView.swift
//  CalculatorApp
//
//  Created by Борис Павлов on 15.06.2024.
//

import UIKit

extension UICollectionView {
    /**
     Регистрация ячеек для UICollectionView.

     - Parameter cells: Переменное количество типов ячеек, которые нужно зарегистрировать.

     Пример использования:
     ```swift
     collectionView.registrationCells(MyCustomCell.self, AnotherCell.self)
     ```
     */
    func registrationCells(_ cells: UICollectionViewCell.Type...) {
        cells.forEach { register($0.self, forCellWithReuseIdentifier: $0.description()) }
    }
    /**
     Dequeue ячейки UICollectionView с использованием класса ячейки.
     
     - Parameter indexPath: IndexPath для dequeuing ячейки.
     - Returns: Ячейка заданного типа.
     
     Пример использования:
     ```swift
     let cell: MyCustomCell = collectionView.dequeueReusableCell(for: indexPath)
     ```
     */
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: T.description(), for: indexPath) as? T else {
            fatalError("Не удалось dequeuing ячейку с идентификатором \(String(describing: T.self))")
        }
        return cell
    }
}
