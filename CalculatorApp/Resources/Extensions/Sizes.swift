//
//  Sizes.swift
//  CalculatorApp
//
//  Created by Борис Павлов on 02.10.2023.
//

import Foundation

struct Sizes {
    struct Spacing {
        static let top: CGFloat = 10.0
        static let left: CGFloat = 10.0
        static let right: CGFloat = -10.0
        static let bottom: CGFloat = -10.0
    }
    
    struct Padding {
        struct SL {
            static let multiplier: CGFloat = 1.0
        }
    }
}
