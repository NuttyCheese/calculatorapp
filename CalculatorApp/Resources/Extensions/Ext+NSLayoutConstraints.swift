//
//  Ext+NSLayoutConstraints.swift
//  CalculatorApp
//
//  Created by Борис Павлов on 15.06.2024.
//

import UIKit

public enum ConstraintsPositionType {
    case topLeft(_ shft: CGFloat = 0)
    case topRight(_ shft: CGFloat = 0)
    case topCenter(_ shft: CGFloat = 0)
    case bottomLeft(_ shft: CGFloat = 0)
    case bottomRight(_ shft: CGFloat = 0)
    case bottomCenter(_ shft: CGFloat = 0)
    case left(_ shft: CGFloat = 0)
    case right(_ shft: CGFloat = 0)
    case center(height: CGFloat? = nil, 
                width: CGFloat? = nil)
    case full(top: CGFloat = 0,
              left: CGFloat = 0,
              right: CGFloat = 0,
              bottom: CGFloat = 0)
}

extension UIView {
    private var superView: UIView { superview! }
    
    public func const(_ type: ConstraintsPositionType, _ view: UIView? = nil) {
        tAMIC()
        
        switch type {
//            case .topLeft(let shift):
//                <#code#>
//            case .topRight(let shift):
//                <#code#>
//            case .topCenter(let shift):
//                <#code#>
//            case .bottomLeft(let shift):
//                <#code#>
//            case .bottomRight(let shift):
//                <#code#>
//            case .bottomCenter(let shift):
//                <#code#>
//            case .left(let shift):
//                <#code#>
//            case .right(let shift):
//                <#code#>
//            case .center(let heigth, let width):
//                <#code#>
            case .full(let top, let left, let right, let bottom):
                NSLayoutConstraint.activate([
                    topAnchor.constraint(equalTo: view?.topAnchor ?? superView.topAnchor, constant: top),
                    leftAnchor.constraint(equalTo: view?.leftAnchor ?? superView.leftAnchor, constant: left),
                    rightAnchor.constraint(equalTo: view?.rightAnchor ?? superView.rightAnchor, constant: right),
                    bottomAnchor.constraint(equalTo: view?.bottomAnchor ?? superView.bottomAnchor, constant: bottom)
                ])
            default: break
        }
    }
}
