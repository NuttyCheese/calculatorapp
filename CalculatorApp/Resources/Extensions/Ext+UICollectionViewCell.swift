//
//  Ext+UICollectionViewCell.swift
//  CalculatorApp
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

extension UICollectionViewCell {
    func basicSettings() {
        layer.cornerRadius = 10
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 1.5
    }
}
