//
//  CellProtocol.swift
//  CalculatorApp
//
//  Created by Борис Павлов on 15.06.2024.
//

import UIKit

protocol ICollectionViewCell where Self: UICollectionViewCell {
    func configure(with model: Any)
}
