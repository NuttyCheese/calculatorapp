//
//  AppDelegate.swift
//  CalculatorApp
//
//  Created by Борис Павлов on 02.10.2023.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        rootToApp()
        return true
    }
}

private extension AppDelegate {
    func rootToApp() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        
        window.rootViewController = UINavigationController(rootViewController: CalculatorViewController())
        window.makeKeyAndVisible()
        
        self.window = window
    }
}
